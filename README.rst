LambdaPy
========

About
-----

This small package is intended as an educational toy to assist in learning the
basics of Lambda Calculus, with a focus on Boolean logic and recursion. To the
best of my knowledge, the basic symbols, functions, and logic presented here
are correct. However, I can make no guarantees, since I am both self-taught,
and in the midst of learning, myself. That said, I appreciate any input from
anyone who knows better and can help shed light on the subject.


I'd also love to hear from anyone regarding improvements that could be made,
both the the code itself (such as there is) and the documentation. This
include syntactical and grammatical issues, as well as installation and usage
problems.


Note about design
-----------------

Python, like most modern programming languages, provides its own syntax for
lambda expressions (see ``help(lambda)``). However, instead of using lambdas
explicitly to define, e.g., the Boolean and combinator definitions in this
package, I have elected to use the standard Python function definition
syntax. I feel this is more pythonic and probably easier to read and
digest. In addition, it also allows for docstrings that look like docstrings,
and saner introspection.

Installing
----------

The code is straightforward, and has no dependencies for normal use. If you
intend to run the bundled tests, pytest and pytest-runner are required. The
setup scripts should take care of that.


The recommended installation method is just to create a simple virtualenv,
clone the repository, and run ``python ./setup.py install`` in the base
directory. LambdaPy uses the default setuptools management system, but if you
intend to run the tests, you'll probably need a fairly recent version of
setuptools installed (in your active virtualenv you can run ``pip
install --upgrade setuptools``). Older versions of setuptools may cause pytest
and/or pytest-runner to fail spectacularly.


Usage
-----

Most everything should have docstrings. Sphinx document generation is not yet
implemented, but it's on the list of future improvements. The package is
currently broken up into two submodules, ``boolean`` and ``combinator``. See
the module docstrings for more information.


Additional resources
--------------------

The following videos and articles may help as an introduction to the topics:

This video on `Lambda Calculus by Computerphile`_

This video on the `Y Combinator by Computerphile`_

The Wikipedia page on `Lambda Calculus`_

The Wikipedia page on the `Fixed-Point Combinator`_

The Wikipedia page on `Boolean Algebra`_

The Wikipedia page on `Recursion`_


.. _Lambda Calculus by Computerphile: https://www.youtube.com/watch?v=eis11j_iGMs
.. _Y Combinator by Computerphile: https://www.youtube.com/watch?v=9T8A89jgeTI
.. _Lambda Calculus: https://en.wikipedia.org/wiki/Lambda_calculus
.. _Fixed-Point Combinator: https://en.wikipedia.org/wiki/Fixed-point_combinator
.. _Boolean Algebra: https://en.wikipedia.org/wiki/Boolean_algebra
.. _Recursion: https://en.wikipedia.org/wiki/Recursion_(computer_science)

  

