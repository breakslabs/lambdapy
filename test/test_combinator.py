import pytest

from lambdapy import *

def test_OMEGA():
    test = lambda x: id(x)
    assert OMEGA(test) == id(test)

@pytest.mark.parametrize('a, x', ((6, 720), (5, 120)))
def test_Y(a, x):
    """Test Y combinator by performing factorials"""
    fac = lambda f: lambda n: (1 if n<2 else n*f(n-1))

    assert Y(fac)(a) == x
