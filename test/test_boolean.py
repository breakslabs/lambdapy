import pytest

from lambdapy import *

TABLE_OR = ((FALSE, FALSE, FALSE), (FALSE, TRUE, TRUE),
                (TRUE, FALSE, TRUE), (TRUE, TRUE, TRUE))
TABLE_AND = ((FALSE, FALSE, FALSE), (FALSE, TRUE, FALSE),
                 (TRUE, FALSE, FALSE), (TRUE, TRUE, TRUE))
TABLE_NOR = ((FALSE, FALSE, TRUE), (FALSE, TRUE, FALSE),
                (TRUE, FALSE, FALSE), (TRUE, TRUE, FALSE))
TABLE_NAND = ((FALSE, FALSE, TRUE), (FALSE, TRUE, TRUE),
                 (TRUE, FALSE, TRUE), (TRUE, TRUE, FALSE))
TABLE_XOR = ((FALSE, FALSE, FALSE), (FALSE, TRUE, TRUE),
                 (TRUE, FALSE, TRUE), (TRUE, TRUE, FALSE))


@pytest.mark.parametrize('a, b, o', ((1, 0, 1), (0, 1, 0)))
def test_TRUE(a, b, o):
    assert TRUE(a, b) == o

@pytest.mark.parametrize('a, b, o', ((1, 0, 0), (0, 1, 1)))
def test_FALSE(a, b, o):
    assert FALSE(a, b) == o

@pytest.mark.parametrize('a, o', ((TRUE, TRUE), (FALSE, FALSE)))
def test_IDENTITY(a, o):
    assert IDENTITY(a) == o

@pytest.mark.parametrize('a, o', ((TRUE, FALSE), (FALSE, TRUE)))
def test_NOT(a, o):
    assert NOT(a) == o

@pytest.mark.parametrize('a, b, o', TABLE_OR)
def test_OR(a, b, o):
    assert OR(a, b) == o

@pytest.mark.parametrize('a, b, o', TABLE_AND)
def test_AND(a, b, o):
    assert AND(a, b) == o

@pytest.mark.parametrize('a, b, o', TABLE_NOR)
def test_NOR(a, b, o):
    assert NOR(a, b) == o

@pytest.mark.parametrize('a, b, o', TABLE_NAND)
def test_NAND(a, b, o):
    assert NAND(a, b) == o
    
@pytest.mark.parametrize('a, b, o', TABLE_XOR)
def test_XOR(a, b, o):
    assert XOR(a, b) == o
