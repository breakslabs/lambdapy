"""Lambda Calculus in Python

This is something of a toy package intended to help people (myself
included) learn about Lambda Calculus. At present, it only implements
some basic Boolean logic building blocks, and a single
combinator. Future versions will include more items in the toy box. In
the mean time, most of the basics are covered here, and there's a lot
of playing to do if you want to. I can also recommend the Wikipedia
pages on Lambda Calaulus and Boolean Logic (especially two-input
logic, which is represented here). In addition, the YouTube channel
'Computerphile' has several episodes on Booleans and Lambda
Calculus. I highly recommend them.
"""

from .boolean import *
from .combinator import *
