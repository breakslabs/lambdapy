"""Boolean logic definitions as pure(ish) Lambda Calculus.

Here are defined the Boolean Logic definitions as Lambda functions. Originally
these were written as actual lambdas using forms like:

   TRUE = lambda a, b: a
   FALSE = lambda a, b: b
   etc...

And while it's entirely possible to do them that way, it's not very
pythonic. And in particular, makes docstrings and introspection ugly. So
herein they are defined as functions, although using a non-pythonic SHOUT
naming convention.

The following 2-input (and two 1-input) functions are defined:

Fundamentals:
   TRUE(a, b) - The Boolean true value
   FALSE(a, b) - The Boolean false value

Basic operations:
   IDENTITY(a) - An identity function, for completeness
   NOT(a) - The Boolean 'not' operation (inversion)
   AND(a, b) - The Boolean 'and' operation
   OR(A, b) - The Boolean 'or' operation

Secondary Operations (universal):
   NAND(a, b) - The Boolean 'not and' operation, which is also a 'universal gate'
   NOR(a, b) - The Boolean 'not or' operation, which is also a 'universal gate'

Remaining Secondary Operations:
   XOR(a, b) - The Boolean binary 'exclusive or' operation
   XNOR(a, b) - The Boolean binary 'exclusive not or' operation

Presently unimplemented Operations:
   Converse nonimplication
   Negation (either operand)
   Material nonimplication
   Projection (either operand)
   Material implication
   Converse implication
Future versions may include these. In the mean time, why not give them a go
yourself?

The Tautology and Contradiction operations are also not defined as such. Can
you explain why? (Hint, they are implemented)

.. note::
   N.B. There are alternate ways to define many of these functions. I attempted
   to chose the most obvious over the fastest/cleverest/most elegant because this
   package is intended as a learning aid.
"""

def TRUE(a, b):
    """The true value definition.

    :param a: Operand a
    :param b: Operand b
    :type a: any
    :type b: any
    :return: Always returns the first operand
    :rtype: any

    One of two primary Lambda definitions, the other being FALSE. From these
    two basic bindings, we can define any other operation. That is to say, any
    Turing machine can be described using some combination of these two
    functions. Any pure function that can be written in any other programming
    language can be written as some combination of TRUE and FALSE, although
    not usually very efficiently.

    It may initially seem pointless to define a function which only ever
    returns the first operand (argument), but it might help to consider it as
    a form of conditional. This can be see in the implementation of the
    Boolean operations provided here. Also keep in mind that operands (in
    Python, arguments) can themselves be functions. That is TRUE(TRUE, FALSE)
    will always return TRUE. So the Python statement holds:
    
    >>> TRUE(TRUE, FALSE) == TRUE
    True
    >>>
    """
    return a

def FALSE(a, b):
    """The false value definition.

    :param a: Operand a
    :param b: Operand b
    :type a: any
    :type b: any
    :return: Always returns the second operand
    :rtype: any

    One of two primary Lambda definitions, the other being TRUE. From these
    two basic bindings, we can define any other operation. That is to say, any
    Turing machine can be described using some combination of these two
    functions. Any pure function that can be written in any other programming
    language can be written as some combination of TRUE and FALSE, although
    not usually very efficiently.

    It may initially seem pointless to define a function which only ever
    returns the second operand (argument), but it might help to consider it as
    a form of conditional. This can be see in the implementation of the
    Boolean operations provided here. Also keep in mind that operands (in
    Python, arguments) can themselves be functions. That is TRUE(TRUE, FALSE)
    will always return TRUE. So the Python statement holds:
    
    >>> TRUE(TRUE, FALSE) == TRUE
    True
    >>>
    """
    return b

def IDENTITY(a):
    """An identity operation

    :param a: Operand a
    :type a: func (one of TRUE or FALSE)
    :return: Returns itself
    :rtype: func
    
    For purposes of completeness, here is an identity function. It always
    returns the given operand.

    a -> a
    """
    return a

def NOT(a):
    """Return the inverse of the operand.

    :param a: Operand a
    :type a: func (One of TRUE or FALSE)
    :return: The inverse of the operand (TRUE->FALSE, FALSE->TRUE)
    :rtype: func

    Inverts the operand. Here you can see that operands are also themselves
    Lambda expressions.

    a -> a(FALSE, TRUE)
    """
    return a(FALSE, TRUE)

def AND(a, b):
    """Return the logical AND of a and b.

    :param a: Operand a
    :param b: Operand b
    :type a: func (One of TRUE or FALSE)
    :type b: func (One of TRUE or FALSE)
    :return: The logical and of a and b
    :rtype: func

    Returns TRUE iff a and b are both TRUE, else FALSE
    The truth table for this operation:
    
    | a | b | out |
    +---+---+-----+
    | 0 | 0 |  0  |
    | 0 | 1 |  0  |
    | 1 | 0 |  0  |
    | 1 | 1 |  1  |

    a, b -> a(b, FALSE)
    """
    return a(b, FALSE)

def OR(a, b):
    """Return the logical OR of a and b.

    :param a: Operand a
    :param b: Operand b
    :type a: func (One of TRUE or FALSE)
    :type b: func (One of TRUE or FALSE)
    :return: The logical or of a and b
    :rtype: func
    
    Returns TRUE if either a or b are TRUE, else FALSE
    The truth table for this operation:
    
    | a | b | out |
    +---+---+-----+
    | 0 | 0 |  0  |
    | 0 | 1 |  1  |
    | 1 | 0 |  1  |
    | 1 | 1 |  1  |

    a, b -> a(TRUE, b)
    """
    return a(TRUE, b)

def NAND(a, b):
    """Return the logical NOT-AND of a and b.

    :param a: Operand a
    :param b: Operand b
    :type a: func (One of TRUE or FALSE)
    :type b: func (One of TRUE or FALSE)
    :return: The logical nand of a and b
    :rtype: func
    
    Returns TRUE iff a or b is FALSE,  else FALSE. Here we can start to see
    more clearly how these functions can be combined to produce other
    functions.
    The truth table for this operation:
    
    | a | b | out |
    +---+---+-----+
    | 0 | 0 |  1  |
    | 0 | 1 |  1  |
    | 1 | 0 |  1  |
    | 1 | 1 |  0  |

    a, b -> NOT(AND(a, b))
    """
    return NOT(AND(a, b))

def NOR(a, b):
    """Return the logical NOT-OR of a and b.

    :param a: Operand a
    :param b: Operand b
    :type a: func (One of TRUE or FALSE)
    :type b: func (One of TRUE or FALSE)
    :return: The logical nor of a and b
    :rtype: func
    
    Returns TRUE if neither a nor b are TRUE, else FALSE
    The truth table for this operation:
    
    | a | b | out |
    +---+---+-----+
    | 0 | 0 |  1  |
    | 0 | 1 |  0  |
    | 1 | 0 |  0  |
    | 1 | 1 |  0  |

    a, b -> NOT(OR(a, b))
    """
    return NOT(OR(a, b))

def XOR(a, b):
    """Return the logical EXCLUSIVE-OR of a and b.

    :param a: Operand a
    :param b: Operand b
    :type a: func (One of TRUE or FALSE)
    :type b: func (One of TRUE or FALSE)
    :return: The logical xor of a and b
    :rtype: func
    
    Returns TRUE if a or b but not both is TRUE, else FALSE
    The truth table for this operation:
    
    | a | b | out |
    +---+---+-----+
    | 0 | 0 |  0  |
    | 0 | 1 |  1  |
    | 1 | 0 |  1  |
    | 1 | 1 |  0  |

    a, b -> a(NOT(b), b)
    """
    return a(NOT(b), b)

def XNOR(a, b):
    """Return the logical EXCLUSIVE-NOT-OR of a and b.

    :param a: Operand a
    :param b: Operand b
    :type a: func (One of TRUE or FALSE)
    :type b: func (One of TRUE or FALSE)
    :return: The logical xnor of a and b
    :rtype: func
    
    Returns TRUE if a and b are both FALSE or both TRUE, else FALSE
    The truth table for this operation:
    
    | a | b | out |
    +---+---+-----+
    | 0 | 0 |  1  |
    | 0 | 1 |  0  |
    | 1 | 0 |  0  |
    | 1 | 1 |  1  |

    a, b -> NOT(XOR(a, b))
    """
    return NOT(XOR(a, b))
