"""Combinatorial calculus functions

This is where the magic starts. Combinators are a little beyond my ability to
explain in a meaningful way. The best I can offer is that the pair presented
here are the root of recursion in Lambda Calculus, which allows for
looping. Combined with the Boolean functions, from which conditionals can be
built, they provide powerful programming tools.

Defined here:

OMEGA(a) - The Omega (ώ) combinator.
Y(a) - The Y Combinator
"""

def OMEGA(a):
    """The Omega combinator.

    :param a: A lambda expression
    :type a: func
    :return: a(a)
    :rtype: func

    Applies an expression to itself.
    """
    return a(a)

def Y(a):
    """The Y combinator, which encodes recursion.

    :param a: Input function
    :type a: func
    :return: Iterative output function
    :rtype: func

    Being honest, I don't have a strong enough grasp on this one yet to feel
    comfortable trying to explain it to someone else without unintentionally
    misleading them. I can show that it works, however, and you can examine it
    and attempt to draw your own conclusions.

    >>> fac = lambda f: lambda n: (1 if n<2 else n*f(n-1))
    >>> Y(fac)(5) == 120
    True
    >>>

    Note here that Y(...) returns a function, which is then passed the argument '5'.
    """
    return OMEGA(lambda f: a(lambda x: OMEGA(f)(x)))

from . import boolean

